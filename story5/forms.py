from django import forms

from .models import Course


class CourseForm(forms.ModelForm):
    class Meta:
        model = Course
        fields = ['name', 'lecturer', 'sks', 'term', 'year', 'room', 'desc']
        widgets = {
            'name' : forms.TextInput(attrs={'class': 'form-control'}),
            'lecturer' : forms.TextInput(attrs={'class': 'form-control'}),
            'sks' : forms.NumberInput(attrs={'class': 'form-control'}),
            'term' : forms.Select(attrs={'class': 'form-control'}),
            'year' : forms.Select(attrs={'class': 'form-control'}),
            'room' : forms.TextInput(attrs={'class': 'form-control'}),
            'desc' : forms.Textarea(attrs={'class': 'form-control'}),
        }