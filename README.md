# COOLYEAH
PPW Odd 2020/2021 - Story 5

## Overview
COOLYEAH is a web application that can be used to organize college schedules. User can add courses that he/she takes in current semester or upcoming semesters. Information such as name, lecturer, term, room, and credits are also shown. User can remove courses provided that there has been some misinput or the course has been passed. Unfortunately, edit course feature has not been implemented in this application.

## Link
Click <http://jojostory5.herokuapp.com> to get to the website.

## Resources
- HTML
- CSS
- Bootstrap
- Django
- Heroku